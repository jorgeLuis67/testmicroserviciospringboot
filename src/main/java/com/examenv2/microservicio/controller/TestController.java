package com.examenv2.microservicio.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

	@GetMapping(value = "/")
	public String testApi() {
		return "Prueba del Microservicio en OpenShif";
	}
}
