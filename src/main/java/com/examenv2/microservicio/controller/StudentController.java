package com.examenv2.microservicio.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.examenv2.microservicio.model.Student;

@RestController
@RequestMapping("/context/students")
//@RequestMapping("/students")
public class StudentController {

	public Student read(@PathVariable String id) {
        //return service.find(id);
		return new Student("Jorge Luis", "Tepoz Ortega");
    }
	
	@GetMapping(value = "/{id}")
	public Student read2(@PathVariable("id") String id) {
        //return service.find(id);
		//throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "test Bad request....");
		return new Student("Jorge Luis", "Tepoz Ortega");
    }
	
}
