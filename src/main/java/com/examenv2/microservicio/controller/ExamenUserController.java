package com.examenv2.microservicio.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.examenv2.microservicio.exception.InformationNotFoundException;
import com.examenv2.microservicio.model.User;
import com.examenv2.microservicio.service.UserService;

@RestController
public class ExamenUserController {
	
	@Autowired
	private UserService userService;

	@RequestMapping(method = RequestMethod.GET, path = "/examen/c2s/users", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<List<User>> recoverUsersFromAPIRegres(@RequestParam(value="page", required = false) String page) {
		
		Integer numPage = new Integer(1); 
		try {
			if(page != null)
				numPage = Integer.valueOf(page);
		} catch(Exception e) {
			numPage = new Integer(3);
		}
		
		List<User> users = userService.getAllUserRegresByPage_Feing(numPage);
		
		if(users == null || users.size() == 0)
			throw new InformationNotFoundException(HttpStatus.NOT_FOUND, "No existe contenido");
		
		return ResponseEntity.ok(users);
	}
	
}
