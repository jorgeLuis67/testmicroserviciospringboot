package com.examenv2.microservicio.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class InformationNotFoundException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	private final HttpStatus httpstatus;
	private final String moreDetail;

	public InformationNotFoundException() {
		super();
		this.httpstatus = null;
		this.moreDetail = null;		
	}
	
	public InformationNotFoundException(String message) {
		super(message);
		this.httpstatus = null;
		this.moreDetail = null;		
	}
	
	public InformationNotFoundException(HttpStatus httpstatus, String moreDetail) {
		super();
		this.httpstatus = httpstatus;
		this.moreDetail = moreDetail;
	}

	public HttpStatus getHttpstatus() {
		return httpstatus;
	}

	public String getMoreDetail() {
		return moreDetail;
	}
		
}
