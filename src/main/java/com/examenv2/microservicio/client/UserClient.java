package com.examenv2.microservicio.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

import com.examenv2.microservicio.model.ClientResponse;

@FeignClient(name = "regres-api", url = "https://reqres.in")
public interface UserClient {

	@GetMapping(value = "/api/users")
	ResponseEntity<ClientResponse> getUsersFromExternalAPIRegres(
				@RequestHeader("user-agent") String userAgent,
				@RequestParam(value="page", required = false) Integer page);
}
