package com.examenv2.microservicio.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.examenv2.microservicio.client.UserClient;
import com.examenv2.microservicio.model.ClientResponse;
import com.examenv2.microservicio.model.User;
import com.examenv2.microservicio.service.UserService;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Service
public class UserServiceImpl implements UserService {
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private UserClient userClient;
	
	@Override
	public List<User> getAllUserRegresByPage_Feing(Integer numPage) {
		String userAgent = "Application";

		logger.info("**** Version - getAllUserRegresByPage_Feing ****");
		
		List<User> userList = null;
		
		ClientResponse response = userClient.getUsersFromExternalAPIRegres(userAgent, numPage).getBody();
		logger.info(response.toString());
		
		if(response != null) {
			userList = response.getData(); 
		}
		
		return userList;
	}
			
}
