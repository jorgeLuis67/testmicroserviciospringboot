package com.examenv2.microservicio.model;

public class Student {
	private String firtsname;
	private String lastname;

	public Student() {
		
	}

	public Student(String firtsname, String lastname) {
		super();
		this.firtsname = firtsname;
		this.lastname = lastname;
	}

	public String getFirtsname() {
		return firtsname;
	}

	public void setFirtsname(String firtsname) {
		this.firtsname = firtsname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	@Override
	public String toString() {
		return "Student [firtsname=" + firtsname + ", lastname=" + lastname + "]";
	}

}
