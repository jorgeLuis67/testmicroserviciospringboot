package com.examenv2.microservicio;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class ExamenV2MicroservicioApplication {

	//NOTA: este proyecto de spring Boot fue creado con STS (No con spring Initializ)
	public static void main(String[] args) {
		SpringApplication.run(ExamenV2MicroservicioApplication.class, args);
	}
	
}
